FROM adoptopenjdk/openjdk11
ADD build/libs/demo-0.0.1-SNAPSHOT.jar demo.jar
ENV JAVA_OPTS=""
ENTRYPOINT ["java","-jar","/demo.jar"]